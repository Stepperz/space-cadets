package core;

/**
 * Created by Ollie on 08/10/14.
 */
public class SubProgram {

    private String progName;

    public SubProgram(String progName){
        this.progName = progName;
    }

    public void run(){

    }

    public String getName(){
        return progName;
    }

}
