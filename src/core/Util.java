package core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.System.out;

/**
 * Created by Ollie on 08/10/14.
 */
public class Util {

        //Receives input from the console.
        public static String getInput(String message){
            out.println(message);
            out.print("> ");

            String in = null;
            try{
                //Create a new bufferedReader from input stream 'System.in'.
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

                //Get the input line.
                in = reader.readLine();
            } catch (IOException e){
                e.printStackTrace();
            }
            return in;
        }
}
