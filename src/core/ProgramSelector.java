package core;

import programs.Demo1;
import programs.Demo2;
import programs.Email2Name;

import java.util.ArrayList;

import static java.lang.System.*;

/**
 * Created by Ollie on 08/10/14.
 */
public class ProgramSelector {

    private ArrayList<SubProgram> programs = new ArrayList<SubProgram>();

    public void init(){
        programs.add(new Email2Name());
        programs.add(new Demo1());
        programs.add(new Demo2());
    }

    public void run(){
        while(true){
            out.println();
            out.println("---------------------------------");
            out.println("------Space Cadets Programs------");
            out.println("---------------------------------");
            out.println();
            for(int i = 0; i < programs.size(); i++){
                out.println(i + " = " + programs.get(i).getName());
            }
            out.println();

            int in = 0;
            try{
                String s = Util.getInput("Type corresponding id number to run a program.\nOr type 'EXIT' to close the program.");
                if(s.equals("EXIT"))
                    break;

                in = Integer.parseInt(s);
                if(in >= programs.size())
                    throw new NumberFormatException();
            }catch(NumberFormatException e){
                out.println("Invalid Program Id.");
                out.println();
                continue;
            }

            programs.get(in).run();
        }
    }

    public static void main(String[] args){
        ProgramSelector prog = new ProgramSelector();
        prog.init();
        prog.run();
    }

}
