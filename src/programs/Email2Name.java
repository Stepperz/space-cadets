package programs;

import core.SubProgram;
import core.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import static java.lang.System.out;

/**
 * Created by Ollie on 08/10/14.
 */
public class Email2Name extends SubProgram {

    private final String URL_BASE = "http://www.ecs.soton.ac.uk/people/";
    private String id;
    private String name;

    public Email2Name() {
        super("Email2Name");
    }

    @Override
    public void run(){
        boolean success;

        while(true){
            success = true;
            id = Util.getInput("Please Enter a valid email id.\nOr type 'BACK' to return to the program selector.");


            //Checks for exit command.
            if(id.equals("BACK")){
                out.println("Closing program...");
                return;
            }

            //Concatenate the input with the base URL to create the full URL.
            String in = URL_BASE + id;
            out.println("Searching for webpage '" + in + "'...");

            try{
                URL url = new URL(in);      //Create the url object using the calculated url address.
                BufferedReader htmlStream = new BufferedReader(new InputStreamReader(url.openStream()));    //Open the stream to receive html data.

                String line;
                String[] tokens;

                //Loop while we still have readable lines.
                while((line = htmlStream.readLine()) != null){
                    //If the line ends with the '</title>' tag then we have found the required line.
                    if(line.endsWith("</title>")){
                        tokens = line.split("<title>");     //Split the line using the '<title>' tag.
                        tokens = tokens[1].split(" ");      //Split the second half of the line using spaces.
                        name = tokens[0];                   //Make 'name equal to the first word in the title.

                        //If the current name equals 'ECS' then the url has returned the default page. Therefore no name will be found later.
                        if(name.equals("ECS")){
                            out.println("No User with email id: " + id + ".");
                            //Break from while loop to prevent unnecessary program execution.
                            break;
                        }

                        //Successively concatenate the remaining tokens onto 'name' until we come across the '|' character.
                        for(int i = 1; i < tokens.length; i++){
                            if(tokens[i].equals("|")){
                                break;
                            }
                            name = name + " " + tokens[i];
                        }

                        //Print out the result and break out of the while loop.
                        out.println("User with email id: '" + id + "' is " + name + ".");
                        out.println();
                        break;
                    }
                }
                //Catch any thrown exceptions and set success to false;
            }catch (MalformedURLException e){
                e.printStackTrace();
                success = false;
            }catch (IOException e){
                e.printStackTrace();
                success = false;
            }

            //If the program failed, inform the user.
            if(!success){
                out.println("Could not find valid webpage '" + in + "'.");
                out.println();
                continue;
            }
        }
    }
}
