package programs;

import core.SubProgram;
import core.Util;

import static java.lang.System.out;

/**
 * Created by Ollie on 08/10/14.
 */
public class Demo1 extends SubProgram {

    public Demo1() {
        super("Demo 1");
    }

    @Override
    public void run(){

        out.println("------Demo 1------");
        out.println();

        while(true){
            String in = Util.getInput("This program does nothing.\nType 'BACK' to return to the program selector.");

            //Checks for exit command.
            if(in.equals("BACK")){
                out.println("Closing program...");
                return;
            }
        }
    }

}
