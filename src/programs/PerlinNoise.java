package programs;

import core.SubProgram;

import java.util.Random;

/**
 * Created by Ollie on 08/10/14.
 */
public class PerlinNoise extends SubProgram {

    private char[][] screenData = new char[50][50];
    private float[][] data = new float[50][50];
    private Random rand;

    public PerlinNoise() {
        super("Perlin Noise Demo");

        rand = new Random();
    }

    @Override
    public void run(){

    }

    private void genData(){

    }

    private void draw(){

    }

    private float lerp(float v1, float v2, float factor){
        return ((v2-v1)*factor)+v1;
    }

}
